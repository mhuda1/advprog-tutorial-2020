package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> SpellList;


    public ChainSpell(ArrayList<Spell> SpellList){
        this.SpellList = SpellList;
    }

    public void cast(){
        for (Spell spell : SpellList){
            spell.cast();
        }
    }

    @Override
    public void undo(){
        for (int i = SpellList.size() -1; i >= 0; i--){
            SpellList.get(i).undo();
        }
    }


    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
