package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    @Override
    public String attack() {
        return "WZAHH!!";
    }

    @Override
    public String getType() {
        return "magic";
    }
}
