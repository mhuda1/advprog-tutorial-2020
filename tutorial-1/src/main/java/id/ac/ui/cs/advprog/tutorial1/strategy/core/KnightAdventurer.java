package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    @Override
    public String getAlias() {
        return "Knight";
    }

    public KnightAdventurer(){
        AttackBehavior attackBehavior = new AttackWithSword();
        DefenseBehavior defenseBehavior = new DefendWithArmor();
        this.setAttackBehavior(attackBehavior);
        this.setDefenseBehavior(defenseBehavior);
    }
}
