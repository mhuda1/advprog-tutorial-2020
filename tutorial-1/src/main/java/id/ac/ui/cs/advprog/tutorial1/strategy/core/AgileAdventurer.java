package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {

    @Override
    public String getAlias() {
        return "Agile";
    }

    public AgileAdventurer(){
        AttackBehavior attackBehavior = new AttackWithGun();
        DefenseBehavior defenseBehavior = new DefendWithBarrier();
        this.setAttackBehavior(attackBehavior);
        this.setDefenseBehavior(defenseBehavior);
    }
}
