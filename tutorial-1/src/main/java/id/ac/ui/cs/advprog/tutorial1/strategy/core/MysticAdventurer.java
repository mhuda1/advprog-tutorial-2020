package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    @Override
    public String getAlias() {
        return "Mystic";
    }

    public MysticAdventurer(){
        AttackBehavior attackBehavior = new AttackWithMagic();
        DefenseBehavior defenseBehavior = new DefendWithShield();
        this.setAttackBehavior(attackBehavior);
        this.setDefenseBehavior(defenseBehavior);
    }
}
