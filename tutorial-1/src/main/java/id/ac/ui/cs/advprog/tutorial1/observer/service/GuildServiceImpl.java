package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {
        private final QuestRepository questRepository;
        private final Guild guild;
        private final Adventurer agileAdventurer;
        private final Adventurer knightAdventurer;
        private final Adventurer mysticAdventurer;

        public GuildServiceImpl(QuestRepository questRepository) {
                this.questRepository = questRepository;
                this.guild = new Guild();

                this.agileAdventurer = new AgileAdventurer(guild);
                this.knightAdventurer = new KnightAdventurer(guild);
                this.mysticAdventurer = new MysticAdventurer(guild);

                this.guild.add(this.agileAdventurer);
                this.guild.add(this.knightAdventurer);
                this.guild.add(this.mysticAdventurer);
        }

        @Override
        public void addQuest(Quest quest) {
                if (this.questRepository.save(quest) != null){
                        this.guild.addQuest(quest);
                }
        }

        @Override
        public List<Adventurer> getAdventurers() {
                return this.guild.getAdventurers();
        }

}
